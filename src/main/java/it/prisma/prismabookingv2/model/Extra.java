package it.prisma.prismabookingv2.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Extra {
	private String id;
	private String name;
	private BigDecimal price;
	private Boolean isDeluxe;
	private List<String> facilityIds;

	@Override
	public boolean equals(Object obj){
		if(obj instanceof Extra){
			return ((Extra) obj).getId().equalsIgnoreCase(this.getId());
		}
		return false;
	}

	public Boolean removeFacilityId(String facilityId){
		return facilityIds.remove(facilityId);
	}

	public boolean hasRequiredFields() {
		return name != null && price != null && isDeluxe != null;
	}
}
