package it.prisma.prismabookingv2.model.room;

public enum  RoomType {
    DELUXE_SUITE,
    JUNIOR_SUITE,
    ECONOMY,
    FAMILY
}
