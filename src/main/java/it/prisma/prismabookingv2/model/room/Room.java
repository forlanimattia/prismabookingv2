package it.prisma.prismabookingv2.model.room;

import it.prisma.prismabookingv2.model.price.Price;
import it.prisma.prismabookingv2.utils.Utilities;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Room {

    private String id;
    private String facilityId;
    private String name;
    private Double size;
    private Integer doubleBedNr ;
    private Integer singleBedNr;
    private Integer bathroomNr ;
    private Boolean hasWhirlpool;
    private RoomType category;
    private Price price;

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Room){
            return ((Room) obj).getId().equalsIgnoreCase(this.getId());
        }
        return false;
    }

    public boolean hasRequiredFields() {
        return facilityId != null && name != null && size != null && doubleBedNr != null && singleBedNr != null &&
                bathroomNr != null && hasWhirlpool != null && category != null && price != null;
    }


}
