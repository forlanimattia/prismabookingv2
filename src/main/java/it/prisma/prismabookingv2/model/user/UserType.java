package it.prisma.prismabookingv2.model.user;

public enum UserType {
    ADMINISTRATOR,
    MANAGER,
    RECEPTIONIST,
    CUSTOMER
}
