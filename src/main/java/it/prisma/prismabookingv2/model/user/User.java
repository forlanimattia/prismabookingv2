package it.prisma.prismabookingv2.model.user;

import it.prisma.prismabookingv2.utils.ConflictException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    private String id;
    private List<String> facilityIds;
    private String email;
    private String firstName;
    private String lastName;
    private UserType type;
    private String phoneNumber;

    @Override
    public boolean equals(Object obj){
        if(obj instanceof User){
            return ((User) obj).getId().equalsIgnoreCase(this.getId());
        }
        return false;
    }

    public void addFacilityId(String facilityId){
        if (facilityIds == null)
            facilityIds = new ArrayList<>();
        Optional.ofNullable(facilityId)
                .map(f -> facilityIds.contains(facilityId) ?
                        new ConflictException(facilityId, "this facility already exists") :
                        facilityIds.add(facilityId));
    }

    public void removeFacilityId(String facilityId){
        if (facilityIds == null)
            facilityIds = new ArrayList<>();
        Optional.ofNullable(facilityId)
                        .map(f -> facilityIds.contains(facilityId) ?
                                facilityIds.remove(facilityId) :
                                new NotFoundException(facilityId));
    }

    public boolean hasRequiredFields() {
        return firstName != null && email != null && lastName != null && type != null && phoneNumber != null;
    }

}
