package it.prisma.prismabookingv2.model.price;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Price {
    private String id;
    private Double lowSeason;
    private Double midSeason;
    private Double highSeason;

    public boolean hasRequiredFields() {
        return lowSeason != null && midSeason != null && highSeason != null;
    }
}
