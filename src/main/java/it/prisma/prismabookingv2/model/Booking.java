package it.prisma.prismabookingv2.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Booking {
	private String id;
	private String userId;
	private String facilityId;
	private String roomId;
	private String serviceId;
	private String startDate;
	private String endDate;

	public boolean hasRequiredFields() {
		return userId != null && facilityId != null && startDate != null && endDate != null;
	}

}
