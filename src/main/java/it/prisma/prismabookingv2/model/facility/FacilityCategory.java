package it.prisma.prismabookingv2.model.facility;

public enum FacilityCategory {
    SPA,
    RESORT,
    HOTEL,
    BNB,
    GUESTHOUSE
}
