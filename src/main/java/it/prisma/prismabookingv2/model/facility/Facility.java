package it.prisma.prismabookingv2.model.facility;

import it.prisma.prismabookingv2.model.Extra;
import it.prisma.prismabookingv2.model.room.Room;
import it.prisma.prismabookingv2.model.user.User;
import it.prisma.prismabookingv2.utils.ConflictException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Facility {

    private String id;
    private String name;
    private String address;
    private String city;
    private String country;
    private String email;
    private List<Extra> extras;
    private Boolean wifi;
    private String phoneNumber;
    private FacilityCategory category;
    private List<User> users = new ArrayList<>();
    private List<Room> rooms = new ArrayList<>();

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Facility) {
            return ((Facility) obj).getId().equalsIgnoreCase(this.getId());
        }
        return false;
    }

    public void removeExtra(Extra extra) {
        Optional.ofNullable(extra)
                .map(ex -> {
                    if (extras.contains(extra)) {
                        return extras.remove(extra);
                    } else throw new NotFoundException(extra.getId());
                });
    }

    public void removeUser(User user) {
        Optional.ofNullable(user)
                .map(ex -> {
                    if (users.contains(user)) {
                        return users.remove(user);
                    } else throw new NotFoundException(user.getId());
                });
    }

    public void removeRoom(Room room) {
        Optional.ofNullable(room)
                .map(r -> {
                    if (rooms.contains(room)) {
                        return rooms.remove(room);
                    } else throw new NotFoundException(room.getId());
                });
    }

    public void addUser(User user) {
        Optional.ofNullable(user)
                .map(use -> {
                    if (users.contains(user)) {
                        throw new ConflictException(user.getId(), "user with this id already exists");
                    } else return users.add(user);
                }).orElseThrow(() -> {
                    assert user != null;
                    return new NotFoundException(user.getId());
                });
    }

    public void addExtra(Extra extra) {
        Optional.ofNullable(extra)
                .map(use -> {
                    if (extras.contains(extra)) {
                        throw new ConflictException(extra.getId(), "user with this id already exists");
                    } else return extras.add(extra);
                }).orElseThrow(() -> {
                    assert extra != null;
                    return new NotFoundException(extra.getId());
                });
    }

    public boolean hasRequiredFields() {
        return name != null && address != null && city != null && country != null && email != null
                && wifi != null && phoneNumber != null && category != null && extras != null
                && users != null && rooms != null;
    }
}
