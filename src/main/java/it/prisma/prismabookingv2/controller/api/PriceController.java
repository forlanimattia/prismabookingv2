package it.prisma.prismabookingv2.controller.api;

import it.prisma.prismabookingv2.model.price.Price;
import it.prisma.prismabookingv2.service.PriceService;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/v1/prices")
public class PriceController {
    private final PriceService priceService;

    public PriceController(PriceService priceService){
        this.priceService = priceService;
    }


    @GetMapping("/{price-id}")
    @ResponseStatus(HttpStatus.OK)
    public Price findPriceById(@PathVariable("price-id") String priceId){
    return priceService.findPrice(priceId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Price postPrice(@RequestBody Price price){
        if(price.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot POST an existing resource");
        return priceService.createPrice(price);
    }

    @PutMapping("/{price-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Price updatePrice(@PathVariable("price-id") String priceId,
                             @RequestBody Price price) {
        return priceService.updatePrice(priceId, price);
    }

    @DeleteMapping("/{price-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePrice(@PathVariable("price-id") String priceId){
        if(!Utilities.nullHandling(priceId) || !Utilities.nullHandling(priceId))
            throw new BadRequestException("Missing some required fields");
        priceService.deletePrice(priceId, true);
    }

}
