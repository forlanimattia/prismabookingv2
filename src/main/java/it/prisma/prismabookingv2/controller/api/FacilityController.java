package it.prisma.prismabookingv2.controller.api;

import it.prisma.prismabookingv2.model.Extra;
import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.model.facility.Facility;
import it.prisma.prismabookingv2.model.user.User;
import it.prisma.prismabookingv2.service.FacilityService;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/facilities")
public class FacilityController {

    private final FacilityService facilityService;
    public FacilityController(FacilityService facilityService) {
        this.facilityService = facilityService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<Facility> findFacilityPage(@RequestParam(required = false) Integer index,
                                                    @RequestParam(required = false) Integer limit){
        return facilityService.findPage(index, limit);
    }

    @GetMapping("/{facility-id}")
    @ResponseStatus(HttpStatus.OK)
    public Facility findFacilityById(@PathVariable("facility-id") String facilityId){
        if (!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing mandatory field");
        return facilityService.findFacilityById(facilityId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Facility createFacility(@RequestBody Facility facility){
        if (!facility.hasRequiredFields()) {
            throw new BadRequestException("facility null");
        }
        return facilityService.createUpdateFacility(facility);
    }

    @PutMapping("/{facility-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Facility updateFacility(@PathVariable("facility-id") String facilityId,
                                   @RequestBody Facility facility){
        if (!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing mandatory field");
        facility.setId(facilityId);
        return facilityService.createUpdateFacility(facility);
    }

    @DeleteMapping("/{facility-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteFacility(@PathVariable("facility-id") String facilityId){
        if (!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing mandatory field");
        facilityService.deleteFacility(facilityId, true);
    }

    @DeleteMapping("/{facility-id}/extras/{extra-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Facility deleteExtraByFacilityId(@PathVariable("facility-id") String facilityId,
                                            @PathVariable("extra-id") String extraId){
        if (!Utilities.nullHandling(facilityId) && !Utilities.nullHandling(extraId))
            throw new BadRequestException("Missing mandatory field");
        return facilityService.deleteExtraByFacilityId(facilityId,extraId);
    }
    @PutMapping("/{facility-id}/extras/{extra-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Extra addExtraByFacilityId(@PathVariable("facility-id") String facilityId,
                                      @PathVariable("extra-id") String extraId) {
        return facilityService.addExtraByFacilityId(facilityId, extraId);
    }

    @DeleteMapping("/{facility-id}/users/{user-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Facility deleteUserByFacilityId(@PathVariable("facility-id") String facilityId,
                                           @PathVariable("user-id") String userId){
        if (!Utilities.nullHandling(facilityId) && !Utilities.nullHandling(userId))
            throw new BadRequestException("Missing mandatory field");
        return facilityService.deleteUserByFacilityId(facilityId, userId);
    }
    @PutMapping("/{facility-id}/users/{user-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public User addUserByFacilityId(@PathVariable("facility-id") String facilityId,
                                    @PathVariable("user-id") String userId) {
        if (!Utilities.nullHandling(facilityId) && !Utilities.nullHandling(userId))
            throw new BadRequestException("Missing mandatory field");
        return facilityService.addUserByFacilityId(facilityId, userId);
    }
}
