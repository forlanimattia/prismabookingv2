package it.prisma.prismabookingv2.controller.api;

import it.prisma.prismabookingv2.model.Extra;
import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.service.ExtraService;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/v1")
public class ExtraController {

    private final ExtraService extraService;

    public ExtraController(ExtraService extraService){this.extraService = extraService;}

    @GetMapping("/extras")
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<Extra> findExtraPage(@RequestParam(required = false) Integer index,
                                              @RequestParam(required = false) Integer limit){
        return extraService.findPage(index, limit);
    }

    @GetMapping("/extras/{extra-id}")
    @ResponseStatus(HttpStatus.OK)
    public Extra findExtraById(@PathVariable("extra-id") String extraId){
        return extraService.findExtraById(extraId);
    }



    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Extra createExtra(@RequestBody Extra extra){
        if(extra.getId() != null)
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot Post an existing Service");
        return extraService.createUpdateExtra(extra);
    }

    @PutMapping("/extras/{extra-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Extra updateExtra(@PathVariable("extra-id") String extraId,
                                 @RequestBody Extra extra){
        extra.setId(extraId);
        return extraService.createUpdateExtra(extra);
    }

    @DeleteMapping("/extras/{extra-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteExtra(@PathVariable("extra-id") String extraId){
        if(!Utilities.nullHandling(extraId) || !Utilities.nullHandling(extraId))
            throw new BadRequestException("Missing some required fields");
        extraService.deleteExtra(extraId, true);
    }

    @GetMapping("/facilities/{facility-id}/extras")
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<Extra> findExtrasByFacilityId (@PathVariable("facility-id") String facilityId,
                                                        @RequestParam(required = false) Integer index,
                                                        @RequestParam(required = false) Integer limit) {
        return extraService.findExtrasByFacilityId(facilityId, index, limit);
    }
}
