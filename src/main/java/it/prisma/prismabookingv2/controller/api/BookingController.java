package it.prisma.prismabookingv2.controller.api;


import it.prisma.prismabookingv2.model.Booking;
import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.service.BookingService;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/facilities/{facility-id}/bookings")
public class BookingController {

    private final BookingService bookingService;

    public BookingController(BookingService bookingService){
        this.bookingService = bookingService;
    }
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<Booking> findBookingPage(@PathVariable("facility-id") String facilityId,
                                                     @RequestParam(required = false) Integer index,
                                                     @RequestParam(required = false) Integer limit){
        if(!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing some required fields");

        return bookingService.findBookingsByFacilityId(facilityId, index, limit);
    }

    @GetMapping("/{booking-id}")
    @ResponseStatus(HttpStatus.OK)
    public Booking findBooking(@PathVariable("facility-id") String facilityId,
                               @PathVariable("booking-id") String bookingId){

        if(!Utilities.nullHandling(facilityId) || !Utilities.nullHandling(bookingId))
            throw new BadRequestException("Missing some required fields");

        return bookingService.findBooking(facilityId, bookingId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Booking createBooking(@PathVariable("facility-id") String facilityId,
                                 @RequestBody Booking booking){

        if(!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing some required fields");

        if(booking == null)
            throw new BadRequestException("Missing request Body");

        if(booking.getId() != null)
            throw new BadRequestException("Cannot POST an existing resource");

        if (booking.hasRequiredFields())
            return bookingService.createBooking(facilityId, booking);
        else throw new BadRequestException("Missing mandatory fields for booking object.");

    }

    @PutMapping("/{booking-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Booking updateBooking(@PathVariable("facility-id") String facilityId,
                                 @PathVariable("booking-id") String bookingId,
                                 @RequestBody Booking booking) {

        if(!Utilities.nullHandling(facilityId) || !Utilities.nullHandling(bookingId))
            throw new BadRequestException("Missing some required fields");

        if(booking == null)
            throw new BadRequestException("Missing request body");

        if(booking.getId() == null)
            throw new BadRequestException("The id can not be null");

        if (booking.hasRequiredFields())
            return bookingService.updateBooking(facilityId, bookingId, booking);
        else
            throw new BadRequestException("Missing mandatory fields for Room object.");
    }

    @DeleteMapping("/{booking-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBooking(@PathVariable("facility-id") String facilityId,
                              @PathVariable("booking-id") String bookingId){

        if(!Utilities.nullHandling(facilityId) || !Utilities.nullHandling(bookingId))
            throw new BadRequestException("Missing some required fields");

        bookingService.deleteFromFile(facilityId, bookingId, true);
    }
}
