package it.prisma.prismabookingv2.controller.api;


import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.model.room.Room;
import it.prisma.prismabookingv2.service.RoomService;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/facilities/{facility-id}/rooms")
public class RoomController {

    private final RoomService roomService;

    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<Room> findRoomPage(@PathVariable("facility-id") String facilityId,
                                            @RequestParam(required = false) Integer index,
                                            @RequestParam(required = false) Integer limit){
        if (!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing mandatory field");
        return roomService.findRoomsByFacilityId(facilityId, index, limit);
    }

    @GetMapping("/{room-id}")
    @ResponseStatus(HttpStatus.OK)
    public Room findRoomById(@PathVariable("facility-id") String facilityId,
                             @PathVariable("room-id") String roomId){
        if (!Utilities.nullHandling(facilityId) && !Utilities.nullHandling(roomId))
            throw new BadRequestException("Missing mandatory field");
        return roomService.findRoomById(facilityId, roomId);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Room createRoom(@PathVariable("facility-id") String facilityId,
                           @RequestBody Room room) {

        if(room.getId() != null)
            throw new BadRequestException("Cannot POST an existing Room.");
        if (!room.hasRequiredFields())
            throw new BadRequestException("Missing mandatory fields for Room object.");
        return roomService.createUpdateRoom(facilityId, room);
    }
    @PutMapping("/{room-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Room updateRoom(@PathVariable("facility-id") String facilityId,
                           @PathVariable("room-id") String roomId,
                           @RequestBody Room room) {
        if (!Utilities.nullHandling(facilityId) && !Utilities.nullHandling(roomId))
            throw new BadRequestException("Missing mandatory field");
        room.setId(roomId);
        return roomService.createUpdateRoom(facilityId, room);
    }
    @DeleteMapping("/{room-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRoom(@PathVariable("facility-id") String facilityId,
                           @PathVariable("room-id") String roomId){
        if (!Utilities.nullHandling(facilityId) && !Utilities.nullHandling(roomId))
            throw new BadRequestException("Missing mandatory field");
        roomService.deleteRoom(facilityId, roomId, true);
    }
}