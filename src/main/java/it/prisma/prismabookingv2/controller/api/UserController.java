package it.prisma.prismabookingv2.controller.api;

import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.model.user.User;
import it.prisma.prismabookingv2.service.UserService;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping("/api/v1")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    // TODO Implement OpenAPI documentation generator
    @GetMapping("/users")
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<User> findUserPage(@RequestParam(required = false) Integer index,
                                            @RequestParam(required = false) Integer limit) {
        return userService.findPage(index, limit);
    }

    @GetMapping("/users/{user-id}")
    @ResponseStatus(HttpStatus.OK)
    public User findUser(@PathVariable("user-id") String userId) {
        if (!Utilities.nullHandling(userId))
            throw new BadRequestException("Missing mandatory field");
        return userService.findUserById(userId);
    }

    // TODO Keep ResponseStatusException or create custom BadRequestException?
    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody User user) {
        if (user.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot POST an existing resource");
        return userService.createUpdateUser(user);
    }

    @PutMapping("/users/{user-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public User updateUser(@PathVariable("user-id") String userId,
                           @RequestBody User user) {
        if (!Utilities.nullHandling(userId))
            throw new BadRequestException("Missing mandatory field");
        user.setId(userId);
        return userService.createUpdateUser(user);
    }

    @DeleteMapping("/users/{user-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable("user-id") String userId) {
        if (!Utilities.nullHandling(userId))
            throw new BadRequestException("Missing mandatory field");
        userService.deleteUser(userId, true);
    }

    @GetMapping("/facilities/{facility-id}/users")
    @ResponseStatus(HttpStatus.OK)
    public PagedResponse<User> findUserPageByFacilityId(@PathVariable("facility-id") String facilityId,
                                                        @RequestParam(required = false) Integer index,
                                                        @RequestParam(required = false) Integer limit) {
        if (!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing mandatory field");
        return userService.findUsersByFacilityId(facilityId, index, limit);
    }
}
