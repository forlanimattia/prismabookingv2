package it.prisma.prismabookingv2.utils;

import java.util.Optional;

public final class Utilities {
    private Utilities(){}

    public static boolean nullHandling(String id){
       return Optional.ofNullable(id)
               .isPresent();
    }
}
