package it.prisma.prismabookingv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.google.gson.Gson;

@SpringBootApplication
public class PrismaBookingv2Application {

    public static void main(String[] args) {
        SpringApplication.run(PrismaBookingv2Application.class, args);
    }
    @Bean
    public Gson gson(){return new Gson();}

}
