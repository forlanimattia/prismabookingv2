package it.prisma.prismabookingv2.service;

import com.google.gson.Gson;
import it.prisma.prismabookingv2.component.ConfigurationComponent;
import it.prisma.prismabookingv2.model.Extra;
import it.prisma.prismabookingv2.model.facility.Facility;
import it.prisma.prismabookingv2.model.user.User;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class FacilityService extends BaseService<Facility> {

    @Value("classpath:data/facilities.json")
    Resource facilityFile;
    Gson gson;
    ExtraService extraService;
    UserService userService;
    RoomService roomService;

    public FacilityService(ConfigurationComponent configurationComponent,
                           Gson gson,
                           ExtraService extraService,
                           UserService userService,
                           RoomService roomService) {
        super.config = configurationComponent;
        this.gson = gson;
        this.extraService = extraService;
        this.userService = userService;
        this.roomService = roomService;
    }

    public Facility findFacilityById(String facilityId) {
        return Optional.ofNullable(facilityId).map(r ->
                        list.stream()
                                .filter(facility -> facility.getId().equalsIgnoreCase(r))
                                .findAny()
                                .orElseThrow(() -> new NotFoundException(r)))
                .orElseThrow(() -> new BadRequestException("Facility ID not valid."));
    }

    public Facility createUpdateFacility(Facility facility) {
        if (facility.getId() == null) {
            facility.setId(UUID.randomUUID().toString());
        }
        if (list.contains(facility)) {
            deleteFacility(facility.getId(), false);
        }
        list.add(facility);

        bwDoAll(gson, facility);

        return facility;
    }

    public void deleteFacility(String facilityId, Boolean isFinalOperation) {
        Facility facilityToDelete = findFacilityById(facilityId);

        if (isFinalOperation) {
            facilityToDelete.getRooms()
                    .forEach(room -> roomService.deleteRoom(facilityId, room.getId(), false));
            facilityToDelete.getExtras()
                    .forEach(extra -> extraService.deleteExtra(extra.getId(), false));
        }

        list.stream()
                .filter(facility -> facility.getId().equalsIgnoreCase(facilityId))
                .findAny()
                .ifPresent(facility -> list.remove(facility));

        try (Stream<String> persistenceFileLines = Files.lines(persistenceFile)) {
            String newContent = persistenceFileLines.map(line -> gson.fromJson(line, Facility.class))
                    .filter(facility -> !facility.getId().equalsIgnoreCase(facilityId))
                    .map(facility -> gson.toJson(facility).concat(System.lineSeparator()))
                    .collect(Collectors.joining());

            if (bw == null)
                bw = bwOpen();
            bwWrite(newContent);
        } catch (IOException e) {
            log.error("Error accessing the file.");
        } finally {
            bwClose(isFinalOperation);
        }
    }

    public Facility deleteExtraByFacilityId(String facilityId,
                                            String extraId) {

        Facility facilityToUpdate = findFacilityById(facilityId);
        Extra extraToRemove = extraService.findExtraById(extraId);
        facilityToUpdate.removeExtra(extraToRemove);
        extraToRemove.removeFacilityId(facilityId);

        extraService.createUpdateExtra(extraToRemove);
        return createUpdateFacility(facilityToUpdate);
    }

    public Extra addExtraByFacilityId(String facilityId,
                                      String extraId) {

        Facility facilityToUpdate = findFacilityById(facilityId);
        Extra extraToAdd = extraService.findExtraById(extraId);

        facilityToUpdate.addExtra(extraToAdd);

        createUpdateFacility(facilityToUpdate);

        return extraService.createUpdateExtra(extraToAdd);
    }

    public Facility deleteUserByFacilityId(String facilityId,
                                           String userId) {

        Facility facilityToUpdate = findFacilityById(facilityId);
        User userToRemove = userService.findUserById(userId);
        facilityToUpdate.removeUser(userToRemove);
        userToRemove.removeFacilityId(facilityId);

        userService.createUpdateUser(userToRemove);
        return createUpdateFacility(facilityToUpdate);
    }

    public User addUserByFacilityId(String facilityId,
                                    String userId) {

        Facility facilityToUpdate = Optional.ofNullable(facilityId)
                .map(f -> findFacilityById(facilityId))
                .orElseThrow(() -> new NotFoundException(facilityId));
        User userToAdd = userService.findUserById(userId);
        facilityToUpdate.addUser(userToAdd);

        createUpdateFacility(facilityToUpdate);
        userToAdd.addFacilityId(facilityId);
        return userService.createUpdateUser(userToAdd);
    }

    @PostConstruct
    private void init() {
        init(facilityFile, "facilities", gson, Facility.class);
    }
}
