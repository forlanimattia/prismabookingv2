package it.prisma.prismabookingv2.service;

import com.google.gson.Gson;
import it.prisma.prismabookingv2.component.ConfigurationComponent;
import it.prisma.prismabookingv2.model.Booking;
import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import it.prisma.prismabookingv2.utils.Utilities;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class BookingService extends BaseService<Booking> {
    @Value("classpath:data/booking.json")
    Resource bookingFile;
    Gson gson;

    public BookingService(ConfigurationComponent configurationComponent,
                          Gson gson) {
        super.config = configurationComponent;
        this.gson = gson;
    }

    public Booking findBooking(String facilityId, String bookingId){

        return list.stream()
                .filter(b -> b.getFacilityId().equalsIgnoreCase(facilityId))
                .filter(b -> b.getId().equalsIgnoreCase(bookingId))
                .findAny()
                .orElseThrow(()-> new NotFoundException(bookingId));
    }

    public Booking createBooking(String idFacility, Booking booking) {

        if(booking.getId() == null) {
            booking.setFacilityId(idFacility);
            booking.setId(UUID.randomUUID().toString());
        }

        list.add(booking);

        if (bw == null)
            bw = bwOpenAppend();
        bwWrite(gson.toJson(booking).concat(System.lineSeparator()));

        bwClose(true);

        return booking;
    }

    public Booking updateBooking(String facilityId, String bookingId, Booking booking) {

        findBooking(facilityId, bookingId);
        deleteFromFile(facilityId, bookingId, false);
        list.add(booking);
        if (bw == null)
            bw = bwOpen();
        bwWrite(gson.toJson(booking).concat(System.lineSeparator()));

        bwClose(true);

        return booking;
    }

    public void deleteFromFile(String facilityId, String bookingId, Boolean isFinalOperation) {

        findBooking(facilityId, bookingId);
        list.stream()
                .filter(booking -> booking.getId().equalsIgnoreCase(bookingId))
                .findAny()
                .ifPresent( booking -> list.remove(booking));

        try (Stream<String> persistenceFileLines = Files.lines(persistenceFile)) {
            String newContent = persistenceFileLines.map(l -> gson.fromJson(l, Booking.class))
                    .filter(l -> !l.getId().equalsIgnoreCase(bookingId))
                    .map(l -> gson.toJson(l).concat(System.lineSeparator()))
                    .collect(Collectors.joining());
            System.out.println(newContent);
            if (bw == null)
                bw = bwOpen();

            bwWrite(newContent);
        } catch (IOException e) {
            log.error("Error accessing the file.");
        } finally {
            bwClose(isFinalOperation);
        }
    }

    public PagedResponse<Booking> findBookingsByFacilityId(String facilityId, Integer index, Integer limit) {
        if(!Utilities.nullHandling(facilityId))
            throw new BadRequestException("Missing some required fields");

        List<Booking> bookingsByFacilityId = list.stream()
                .filter(b -> b.getFacilityId().equalsIgnoreCase(facilityId))
                .collect(Collectors.toList());
        return findPageMethodBody(index, limit, bookingsByFacilityId);
    }

    @PostConstruct
    private void init() {
        init(bookingFile, "bookings", gson, Booking.class);
    }
}