package it.prisma.prismabookingv2.service;

import com.google.gson.Gson;
import it.prisma.prismabookingv2.component.ConfigurationComponent;
import it.prisma.prismabookingv2.model.Extra;
import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import it.prisma.prismabookingv2.utils.Utilities;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class ExtraService extends BaseService<Extra> {

    @Value("classpath:data/extras.json")
    Resource extraFile;
    Gson gson;

    public ExtraService(ConfigurationComponent configurationComponent,
                        Gson gson) {
        super.config = configurationComponent;
        this.gson = gson;
    }

    public PagedResponse<Extra> findExtrasByFacilityId(String facilityId,
                                                       Integer index,
                                                       Integer limit) {
        List<Extra> extrasByFacilityId = list.stream()
                .filter(e -> e.getFacilityIds().stream()
                        .anyMatch(fId -> fId.equalsIgnoreCase(facilityId)))
                .collect(Collectors.toList());

        return findPageMethodBody(index, limit, extrasByFacilityId);

    }

    public Extra findExtraById(String extraId){
        return Optional.ofNullable(extraId).map(e -> list.stream()
                .filter(extra -> extra.getId().equalsIgnoreCase(extraId))
                .findAny()
                .orElseThrow(() -> new NotFoundException(e)))
                .orElseThrow(() -> new BadRequestException("Extra ID not valid"));

//        return list.stream()
//                .filter(extra -> extra.getId().equalsIgnoreCase(extraId))
//                .findAny()
//                .orElseThrow(() -> new NotFoundException(extraId));
    }

    public Extra createUpdateExtra(Extra extra) {
        if(extra == null){
            throw new BadRequestException("Extra is null");
        }
        if (extra.getId() == null)
            extra.setId(UUID.randomUUID().toString());
        else {
            findExtraById(extra.getId());
            deleteExtra(extra.getId(), false);
        }
        list.add(extra);
        bwDoAll(gson, extra);
        return extra;
    }

    public void deleteExtra(String extraId, Boolean isFinalOperation) {
        findExtraById(extraId);
        list.stream()
                .filter(extra -> extra.getId().equalsIgnoreCase(extraId))
                .findAny()
                .ifPresent(extra -> list.remove(extra));

        try(Stream<String> persistenceFileLines = Files.lines(persistenceFile)) {
            String newContent = persistenceFileLines.map(line -> gson.fromJson(line, Extra.class))
                    .filter(extra -> !extra.getId().equalsIgnoreCase(extraId))
                    .map(extra -> gson.toJson(extra).concat(System.lineSeparator()))
                    .collect(Collectors.joining());
            if (bw==null)
                bw = bwOpen();
            bwWrite(newContent);
        } catch (IOException e) {
            log.error("Error accessing the file.");
        } finally {
            bwClose(isFinalOperation);
        }
    }

    @PostConstruct
    private void init() {
        init(extraFile, "extras", gson, Extra.class);
    }
}
