package it.prisma.prismabookingv2.service;

import com.google.gson.Gson;
import it.prisma.prismabookingv2.component.ConfigurationComponent;
import it.prisma.prismabookingv2.model.price.Price;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import it.prisma.prismabookingv2.utils.Utilities;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
@Service
@Slf4j
public class PriceService extends BaseService<Price>{

    @Value("classpath:data/prices.json")
    Resource priceFile;
    Gson gson;
    BufferedWriter bw;

    public PriceService(ConfigurationComponent configurationComponent,
                        Gson gson ){
        super.config = configurationComponent;
        this.gson = gson;
    }

    public Price findPrice(String priceId){
        return Optional.ofNullable(priceId).map(p -> list.stream()
                        .filter(price -> price.getId().equalsIgnoreCase(p))
                        .findAny()
                        .orElseThrow(() -> new NotFoundException(p)))
                .orElseThrow(() -> new BadRequestException("Price Id not valid."));
    }

    public Price createPrice(Price price){
        if (price == null){
            throw new BadRequestException("facility null");
        }
        if(price.getId()==null){
            price.setId(UUID.randomUUID().toString());
        }
        list.add(price);
        bwDoAll(gson, price);
        return price;
    }

    public Price updatePrice(String id, Price price) {
        if(price.getId() == null)
            throw new BadRequestException("The id can not be null");
        findPrice(id);
        deletePrice(id, false);
        list.add(price);
        bwDoAll(gson, price);
        return price;
    }

    public void deletePrice(String id, Boolean isFinalOperation){
        findPrice(id);
        list.stream().filter(r -> r.getId().equalsIgnoreCase(id))
                        .findAny()
                        .ifPresent(r -> list.remove(r));
        //list.remove(findPrice(id));
        try(Stream<String> persistenceFileLines = Files.lines(persistenceFile)) {
            String newContent = persistenceFileLines.map(l -> gson.fromJson(l, Price.class))
                    //.filter(l -> !l.getId().equalsIgnoreCase(id))
                    .map(l -> gson.toJson(l).concat(System.lineSeparator()))
                    .collect(Collectors.joining());
            if (bw == null){
                bw = bwOpen();
            }

            bwWrite(newContent);
        } catch (IOException e) {
            log.error("Error accessing the file.");
        } finally {
            bwClose(isFinalOperation);
        }
    }

    @PostConstruct
    private void init() {
        init(priceFile, "prices", gson, Price.class);
    }

}



