package it.prisma.prismabookingv2.service;

import com.google.gson.Gson;
import it.prisma.prismabookingv2.component.ConfigurationComponent;
import it.prisma.prismabookingv2.model.PagedResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
public class BaseService<T> {

    protected ConfigurationComponent config;
    protected List<T> list = new ArrayList<>();
    protected Path persistenceFile;
    protected BufferedWriter bw;

    public PagedResponse<T> findPage(Integer index, Integer limit) {
        return findPageMethodBody(index, limit, list);
    }

    protected PagedResponse<T> findPageMethodBody(Integer index, Integer limit, List<T> list) {

        limit = handleLimit(limit, list);
        Integer start = index*limit;

        if(start >= list.size())
            return pagedResponse(start, start + limit, index, new ArrayList<>());

        return pagedResponse(start, start + limit, index, list);
    }

    private PagedResponse<T> pagedResponse (Integer start, Integer end, Integer index, List<T> list){

        List<T> pageContent = list;
        if(list.size() > 0)
            pageContent = list.subList(start, end);
        return PagedResponse.<T>builder()
                .data(pageContent)
                .index(index.longValue())
                .totalElements((long) list.size())
                .build();
    }

    public T addResource(T resource) {
        list.add(resource);
        return resource;
    }

    private Integer handleLimit(Integer limit, List<T> list) {
        limit = Optional.ofNullable(limit)
                .filter(l -> l <= config.getMaxPageLimit())
                .orElse(config.getDefaultPageLimit());

        if(limit > list.size()) {
            limit = list.size();
        }
        log.warn(limit + " limit handle");
        return limit;
    }

    protected BufferedWriter bwOpen() {
        try {
            return new BufferedWriter(new FileWriter(persistenceFile.toFile(), false));
        } catch (IOException e) {
            log.error("The file is a directory OR cannot be created OR cannot be opened.");
        }
        return null;
    }
    protected BufferedWriter bwOpenAppend() {
        try {
            return new BufferedWriter(new FileWriter(persistenceFile.toFile(), true));
        } catch (IOException e) {
            log.error("The file is a directory OR cannot be created OR cannot be opened.");
        }
        return null;
    }


    protected void bwWrite(String content){
        if (bw == null)
            bw = bwOpen();
        try {
            bw.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void bwClose (Boolean isFinalOperation) {
        if (isFinalOperation) {
            try {
                System.out.println(bw);
                this.bw.close();
                this.bw = null;
            } catch (IOException e) {
                log.error("Error accessing the file.");
            }
        }
    }

    protected void init(Resource resource, String className, Gson gson, Type classType) {
        if (!resource.exists())
            return;
        try {
            persistenceFile = Files.createTempFile(className+"_", ".json");
            Files.copy(resource.getFile().toPath(), persistenceFile, StandardCopyOption.REPLACE_EXISTING);
            log.info("Using persistence file for " + className + " with path: {}", persistenceFile);
            try (Stream<String> lines = Files.lines(persistenceFile)) {
                lines.forEach(l -> {
                    try {
                        list.add(gson.fromJson(l, classType));
                    } catch (Exception e) {
                        log.error("Error parsing line with cause: {}", e.getMessage());
                    }
                });
            }
        } catch (IOException e) {
            log.error("Error reading " + className + " file with cause: {}", e.getMessage());
        }
    }

    protected void bwDoAll(Gson gson, T t) {
        if (bw==null)
            bw = bwOpenAppend();
        bwWrite(gson.toJson(t).concat(System.lineSeparator()));
        bwClose(true);
    }
}
