package it.prisma.prismabookingv2.service;

import com.google.gson.Gson;
import it.prisma.prismabookingv2.component.ConfigurationComponent;
import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.model.user.User;
import it.prisma.prismabookingv2.utils.BadRequestException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import it.prisma.prismabookingv2.utils.Utilities;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class UserService extends BaseService<User> {

    @Value("classpath:data/users.json")
    Resource userFile;
    Gson gson;

    public UserService(ConfigurationComponent configurationComponent,
                       Gson gson) {
        super.config= configurationComponent;
        this.gson = gson;
    }

    public User findUserById(String userId){
        return Optional.ofNullable(userId).map(u -> list.stream()
                        .filter(user -> user.getId().equalsIgnoreCase(u))
                        .findAny()
                        .orElseThrow(() -> new NotFoundException(u)))
                .orElseThrow(() -> new BadRequestException("User ID not valid."));
    }

    @SneakyThrows
    public User createUpdateUser(User user){
        if(user.getId() == null){
            user.setId(UUID.randomUUID().toString());
        }
        if(list.contains(user)){
            deleteUser(user.getId(), false );
        }
        list.add(user);

        bwDoAll(gson, user);

        return user;
    }

    @SneakyThrows
    public void deleteUser(String userId, Boolean isFinalOperation){
        findUserById(userId);
        list.stream()
                .filter(u -> u.getId().equalsIgnoreCase(userId))
                .findFirst()
                .ifPresent(u -> list.remove(u));

        try(Stream<String> persistenceFileLines = Files.lines(persistenceFile)) {
            String newContent = persistenceFileLines.map(l -> gson.fromJson(l, User.class))
                    .filter(l -> !l.getId().equalsIgnoreCase(userId))
                    .map(l -> gson.toJson(l).concat(System.lineSeparator()))
                    .collect(Collectors.joining());
            if (bw == null)
                bw = bwOpen();
            bwWrite(newContent);
        } catch (IOException e) {
            log.error("Error accessing the file.");
        } finally {
            bwClose(isFinalOperation);
        }
    }

    public PagedResponse<User> findUsersByFacilityId(String facilityId, Integer index, Integer limit) {

        List<User> usersByFacilityId = list.stream()
                .filter(u -> u.getFacilityIds().stream()
                        .anyMatch(fId -> fId.equalsIgnoreCase(facilityId)))
                .collect(Collectors.toList());

        return findPageMethodBody(index, limit, usersByFacilityId);
    }

    @PostConstruct
    private void init() {
        init(userFile, "users", gson, User.class);
    }
}
