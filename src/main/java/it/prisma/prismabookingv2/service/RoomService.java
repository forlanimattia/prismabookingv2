package it.prisma.prismabookingv2.service;

import com.google.gson.Gson;
import it.prisma.prismabookingv2.component.ConfigurationComponent;
import it.prisma.prismabookingv2.model.PagedResponse;
import it.prisma.prismabookingv2.model.facility.Facility;
import it.prisma.prismabookingv2.model.room.Room;
import it.prisma.prismabookingv2.utils.ConflictException;
import it.prisma.prismabookingv2.utils.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class RoomService extends BaseService<Room> {

   @Value("classpath:data/rooms.json")
   Resource roomFile;
   Gson gson;
   FacilityService facilityService;

    public RoomService(ConfigurationComponent configurationComponent,
                       Gson gson,
                       @Lazy FacilityService facilityService){
        super.config = configurationComponent;
        this.gson = gson;
        this.facilityService = facilityService;
    }

    public PagedResponse<Room> findRoomsByFacilityId(String facilityId,
                                                     Integer index,
                                                     Integer limit) {
        facilityService.findFacilityById(facilityId);

        List<Room> roomsByFacilityId = list.stream()
                .filter(room -> room.getFacilityId().equalsIgnoreCase(facilityId))
                .collect(Collectors.toList());

        return findPageMethodBody(index, limit, roomsByFacilityId);
    }

    public Room findRoomById(String facilityId, String roomId){
        facilityService.findFacilityById(facilityId);

        return Optional.ofNullable(facilityId)
                .map(r ->list.stream()
                    .filter(room -> room.getFacilityId().equalsIgnoreCase(facilityId))
                    .filter(room -> room.getId().equalsIgnoreCase(roomId))
                    .findFirst()
                    .orElseThrow(() -> new NotFoundException(roomId)))
                .orElseThrow(() -> new NotFoundException(roomId));
    }

    public Room createUpdateRoom(String facilityId, Room room) {
        facilityService.findFacilityById(facilityId);
        room.setFacilityId(facilityId);
        if(room.getId()==null){
            room.setId(UUID.randomUUID().toString());
        }
        list.stream()
                .filter(r -> r.getName() != null)
                .filter(r -> r.getName().equalsIgnoreCase(room.getName()))
                .findAny().ifPresentOrElse(r -> {
                    if(r.getId().equalsIgnoreCase(room.getId()))
                        deleteRoom(facilityId,r.getId(), false);
                    else throw new ConflictException("name", room.getName());
                    }, () -> {
                    if(list.contains(room))
                        deleteRoom(facilityId, room.getId(), false);
                    }
                );
        list.add(room);

        bwDoAll(gson, room);

        return room;
    }

    public void deleteRoom(String facilityId, String roomId, Boolean isFinalOperation){
        Facility facilityToUpdate = facilityService.findFacilityById(facilityId);
        Room roomToRemove = findRoomById(facilityId, roomId);
        if (isFinalOperation) {
            facilityToUpdate.removeRoom(roomToRemove);
            facilityService.createUpdateFacility(facilityToUpdate);
        }

        list.stream()
                .filter(r -> r.getId().equalsIgnoreCase(roomId))
                .findFirst()
                .ifPresent(r -> list.remove(r));

        try(Stream<String> persistenceFileLines = Files.lines(persistenceFile)) {
            String newContent = persistenceFileLines.map(l -> gson.fromJson(l, Room.class))
                    .filter(l -> !l.getId().equalsIgnoreCase(roomId))
                    .map(l -> gson.toJson(l).concat(System.lineSeparator()))
                    .collect(Collectors.joining());
            if (bw == null)
                bw = bwOpen();
            bwWrite(newContent);
        } catch (IOException e) {
            log.error("Error accessing the file.");
        } finally {
            bwClose(isFinalOperation);
        }
    }

    @PostConstruct
    private void init() {
        init(roomFile, "rooms", gson, Room.class);
    }

}